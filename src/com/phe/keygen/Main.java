package com.phe.keygen;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        GenericKeyMaker keyMaker = new GenericKeyMaker();
        System.out.println("pyCharm key : \n" + keyMaker.genericPyCharmKey(1, 13, args[0]) + "\n");
        System.out.println("phpStorm key : \n" + keyMaker.genericPhpStormKey(1, 13, args[0]) + "\n");
        System.out.println("RubyMine key : \n" + keyMaker.genericRubyMineKey(1, 13, args[0]) + "\n");
        System.out.println("WebStorm key : \n" + keyMaker.genericWebStormKey(1, 13, args[0]) + "\n");
        Random r = new Random();
        System.out.println("Intellij key : \n" + JIKeyMaker.MakeKey(args[0], 0, r.nextInt(100000)));
    }
}
